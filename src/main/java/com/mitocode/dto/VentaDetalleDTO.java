package com.mitocode.dto;

import com.mitocode.model.DetalleVenta;
import com.mitocode.model.Venta;

public class VentaDetalleDTO {
	private Venta venta;
	private DetalleVenta detalleVenta;
	
	public Venta getVenta() {
		return venta;
	}
	public void setVenta(Venta venta) {
		this.venta = venta;
	}
	public DetalleVenta getDetalleVenta() {
		return detalleVenta;
	}
	public void setDetalleVenta(DetalleVenta detalleVenta) {
		this.detalleVenta = detalleVenta;
	}
}
