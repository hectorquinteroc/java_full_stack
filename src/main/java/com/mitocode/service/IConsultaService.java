package com.mitocode.service;

import com.mitocode.dto.VentaDetalleDTO;
import com.mitocode.model.Venta;

public interface IConsultaService extends ICRUD<Venta, Integer>{
	Venta registrarTransaccional(VentaDetalleDTO dto) throws Exception;
}
