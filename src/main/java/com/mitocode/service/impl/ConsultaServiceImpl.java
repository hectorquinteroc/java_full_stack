package com.mitocode.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mitocode.dto.VentaDetalleDTO;
import com.mitocode.model.Venta;
import com.mitocode.repo.IConsultaRepo;
import com.mitocode.repo.IGenericRepo;
import com.mitocode.repo.IVentaRepo;
import com.mitocode.service.IConsultaService;

@Service
public class ConsultaServiceImpl extends CRUDImpl<Venta, Integer> implements IConsultaService{
	@Autowired
	private IVentaRepo repo;
	
	@Autowired
	private IConsultaRepo iConsultaRepo;
	
	@Override
	protected IGenericRepo<Venta, Integer> getRepo(){
		return repo;
	}
	
	@Transactional
	@Override
	public Venta registrarTransaccional(VentaDetalleDTO dto) throws Exception {
		repo.save(dto.getVenta()); 
		iConsultaRepo.registrar(dto.getDetalleVenta().getIdProducto().getIdProducto(), dto.getVenta().getIdVenta(), dto.getDetalleVenta().getCantidad());
		return dto.getVenta();
	}
}
