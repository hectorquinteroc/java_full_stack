package com.mitocode.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.model.DetalleVenta;
import com.mitocode.repo.IDetalleVentaRepo;
import com.mitocode.repo.IGenericRepo;
import com.mitocode.service.IDetalleVentaService;

@Service
public class DetalleVentaImpl extends CRUDImpl<DetalleVenta, Integer> implements IDetalleVentaService{
	
	@Autowired
	private IDetalleVentaRepo iDetalleVentaRepo;
	
	@Override
	protected IGenericRepo<DetalleVenta, Integer> getRepo(){
		return iDetalleVentaRepo;
	}
	
//	@Override
//	public DetalleVenta registrar(DetalleVenta detalleVenta) {
//		return iDetalleVentaRepo.save(detalleVenta);
//	}
}
