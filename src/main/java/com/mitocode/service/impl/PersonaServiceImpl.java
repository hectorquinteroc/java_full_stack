package com.mitocode.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.model.Persona;
import com.mitocode.repo.IGenericRepo;
import com.mitocode.repo.IPersonaRepo;
import com.mitocode.service.IPersonaService;

@Service
public class PersonaServiceImpl extends CRUDImpl<Persona, Integer> implements IPersonaService{

	@Autowired
	private IPersonaRepo iPersonaRepo;
	
	@Override
	protected IGenericRepo<Persona, Integer> getRepo(){
		return iPersonaRepo;
	}
	
//	@Override
//	public Persona registrar(Persona per) {
//		return iPersonaRepo.save(per);
//	}
//
//	@Override
//	public Persona modificar(Persona per) {
//		return iPersonaRepo.save(per);
//	}
//
//	@Override
//	public List<Persona> listar() {
//		return iPersonaRepo.findAll();
//	}
//
//	@Override
//	public Persona listarPorId(Integer id) {
//		Optional<Persona> op = iPersonaRepo.findById(id);
//		return op.isPresent() ? op.get() : new Persona();
//	}
//
//	@Override
//	public void eliminar(Integer id) {
//		iPersonaRepo.deleteById(id);
//	}
}
