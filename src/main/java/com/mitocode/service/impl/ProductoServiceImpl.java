package com.mitocode.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.model.Producto;
import com.mitocode.repo.IGenericRepo;
import com.mitocode.repo.IProductoRepo;
import com.mitocode.service.IProductoService;

@Service
public class ProductoServiceImpl extends CRUDImpl<Producto, Integer> implements IProductoService{

	@Autowired
	IProductoRepo iProductoRepo;

	@Override
	protected IGenericRepo<Producto, Integer> getRepo(){
		return iProductoRepo;
	}
	
//	@Override
//	public Producto registrar(Producto prod) {
//		return iProductoRepo.save(prod);
//	}
//
//	@Override
//	public Producto modificar(Producto prod) {
//		return iProductoRepo.save(prod);
//	}
//
//	@Override
//	public List<Producto> listar() {
//		return iProductoRepo.findAll();
//	}
//
//	@Override
//	public Producto listarPorId(Integer id) {
//		Optional<Producto> op = iProductoRepo.findById(id);
//		return op.isPresent() ? op.get() : new Producto();
//	}
//
//	@Override
//	public void eliminar(Integer id) {
//		iProductoRepo.deleteById(id);
//	}
}
