package com.mitocode.repo;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mitocode.model.DetalleVenta;

public interface IConsultaRepo extends IGenericRepo<DetalleVenta, Integer>{
	@Modifying
	@Query(value = "INSERT INTO detalle_venta(id_producto, id_venta, cantidad) VALUES (:idProducto, :idVenta, :cantidad)", nativeQuery = true)
	Integer registrar(@Param("idProducto") Integer idProducto, @Param("idVenta") Integer idVenta, @Param("cantidad") Integer cantidad);
}
