package com.mitocode.controller;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mitocode.model.DetalleVenta;
import com.mitocode.service.IDetalleVentaService;

@RestController
@RequestMapping("/detalleVentas")
public class DetalleVentaController {
	@Autowired
	private IDetalleVentaService iDetalleVentaService;
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody DetalleVenta detalleVenta) throws Exception {
		DetalleVenta obj = iDetalleVentaService.registrar(detalleVenta);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdVenta()).toUri();
		return ResponseEntity.created(location).build();
	}
}
